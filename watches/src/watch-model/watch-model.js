class WatchModel {
    constructor (watchName, timeZone){
        this.watchName = watchName;
        this.timeZone = timeZone;
    }
}

export default WatchModel;