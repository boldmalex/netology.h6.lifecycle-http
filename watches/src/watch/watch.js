import react, { useEffect, useState } from "react";

const Watch = ({watchName, timeZoneHours}) =>{

    const [watchTime, setWatchTime] = useState();

    const getDate = (timeZoneHours) =>{
        let date = new Date()

        const offsetInHours = date.getTimezoneOffset() / 60;
        
        const offsetDeltaHours = offsetInHours * 60 * 60 * 1000;
        const currentDeltaHours = timeZoneHours * 60 * 60 * 1000;
        
        const utcDate = new Date(date.getTime() + offsetDeltaHours);

        return new Date(utcDate.getTime() + currentDeltaHours);
    }


    useEffect(() => {
        const intervalId = setInterval(() => {
        setWatchTime(getDate(timeZoneHours));
        }, 1000);
        
        return () => {
            clearInterval(intervalId);
        }
    }, []);


    const showTime = (date) =>{
        var hours = date.getHours();
        var minutes = date.getMinutes();
        var seconds = date.getSeconds();
        return(
            <div>
                {hours < 10 ? '0' + hours : hours}:{minutes < 10 ? '0' + minutes : minutes}:{seconds < 10 ? '0' + seconds : seconds} 
            </div>
        );
    }


    if (watchTime === undefined) return null;
    
    return (
        <div>
            <div name={watchName}>{watchName}</div>
            {showTime(watchTime)}
        </div>
    );
};


export default Watch;