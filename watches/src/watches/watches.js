import react, { useEffect, useState } from "react";
import WatchModel from "../watch-model/watch-model";
import Watch from "../watch/watch";


const Watches = () => {

    const [watches, setWatches] = useState([]);

    
    const onWatchAddHandler = (evt) =>{
        evt.preventDefault();

        const watchName = evt.target["watchName"].value;
        const timeZone = evt.target["timeZone"].value;
        const newWatch = new WatchModel(watchName, timeZone);

        setWatches(last => ([...last, newWatch]));
    };


    const showWatches = (items) => {
        return (
            items.map((item, index) => <Watch key = {index} 
                                              watchName={item.watchName} 
                                              timeZoneHours = {Number(item.timeZone)}/>)
        );
    };

   
    return (
        <div>
            <form onSubmit={onWatchAddHandler}>
                <div>
                    <input name="watchName"></input>
                    <input name="timeZone"></input>
                    <button type="submit">Добавить</button>
                </div>
            </form>
            <div>
                {showWatches(watches)}         
            </div>
        </div>
    )
}

export default Watches;