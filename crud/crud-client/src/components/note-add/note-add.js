import react, { useState } from "react";
import "./note-add.css";

const NoteAdd = ({onAdd}) => {

    const [text, setText] = useState("");

    const onSubmit = (evt) => {
        evt.preventDefault();
        onAdd(text);
        setText("");
    }

    const onChange = ({target}) => {
        const value = target.value;
        setText(value);
    }

    return (
        <form onSubmit={onSubmit}>
            <div className="note-add">
                <input type="text"
                        id="text"
                        name="text"
                        value={text}
                        onChange={onChange}/>
                <button type="submit">Добавить</button> 
            </div>
        </form>
    );
}

export default NoteAdd;