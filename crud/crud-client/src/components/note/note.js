import react from "react";
import "./note.css";


const Note = ({id, content, onRemove}) => {

    const onRemoveClick = (evt) => {
        evt.preventDefault();
        onRemove(id);
    }

    return(
        <div key = {id} className="note" >
            <h5>{content}</h5>
            <button onClick={onRemoveClick}>Удалить</button>
        </div>
    );

}

export default Note;