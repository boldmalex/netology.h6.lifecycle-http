import react, { useEffect, useState } from "react";
import NoteAdd from "../note-add/note-add.js";
import Note from "../note/note.js";
import "./notes.css";


const Notes = () => {

    const [items, setItems] = useState([]);

    useEffect(() => {
        serverLoadNotes();
    }, []);


    const onRemove = (id) => {
        serverRemoveNote(id)
        .then(serverLoadNotes);
    }


    const onAdd = (text) => {
        let content = {content: text};
        serverAddNote(content)
        .then(serverLoadNotes);
    }


    const onReload = () => {
        serverLoadNotes();
    }


    const serverLoadNotes = () => {
       
        return fetch("http://localhost:7777/notes")
        .then(res => res.json())
        .then(res => setItems(res));
    }


    const serverAddNote = (content) => {
        
        return fetch("http://localhost:7777/notes", {
            method: "POST",
            body: JSON.stringify(content),
            headers: {
                "Content-Type":"application/json"
            }
        });
    }


    const serverRemoveNote = (id) => {
        
        return fetch(`http://localhost:7777/notes/${id}`, {
            method: "DELETE",
        });
    }


    const listItems = items.map(item => <Note key = {item.id} 
        id={item.id} 
        content={item.content} 
        onRemove = {onRemove}/>
    );

    
    return (
        <div>
            <button className="reload-button" onClick={onReload}>Обновить</button>
            <div className="notes">
                {listItems}
            </div>
            <NoteAdd onAdd={onAdd}/>
        </div>
    );
    
}

export default Notes;